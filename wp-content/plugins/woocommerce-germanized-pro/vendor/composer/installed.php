<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'f82dd8525941ccdaedb2aa6ae47c82ed81eb314b',
    'name' => 'vendidero/woocommerce-germanized-pro',
  ),
  'versions' => 
  array (
    'automattic/jetpack-autoloader' => 
    array (
      'pretty_version' => '2.10.1',
      'version' => '2.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '20393c4677765c3e737dcb5aee7a3f7b90dce4b3',
    ),
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae03311f45dfe194412081526be2e003960df74b',
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'vendidero/storeabill' => 
    array (
      'pretty_version' => 'v1.7.4',
      'version' => '1.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59887ad9fbd3fea98b5074cbae414358b8a4c4d',
    ),
    'vendidero/storeabill-lexoffice' => 
    array (
      'pretty_version' => 'v1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ed815fc1718f50be2e621e8c001aab9bb197ce16',
    ),
    'vendidero/storeabill-sevdesk' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1bfaf5ff4a7093f18c6003a2f1aa5e29c088970a',
    ),
    'vendidero/woocommerce-germanized-dpd' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b5ab028959672b2c9b9c3a6e00b6846a6915cd87',
    ),
    'vendidero/woocommerce-germanized-pro' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'f82dd8525941ccdaedb2aa6ae47c82ed81eb314b',
    ),
  ),
);
