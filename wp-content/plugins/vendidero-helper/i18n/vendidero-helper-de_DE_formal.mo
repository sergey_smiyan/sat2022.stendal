��            )         �     �  O   �       -   %     S     i  d   z     �     �     �       &   &     M     U  ]   g  �   �  0   G  
   x     �  !   �     �     �  8   �  0     v   =  U   �     
       	   %  I   /  .  y     �	  K   �	     
  (   "
     K
     i
  o   ~
     �
     �
  "        4  7   E     }     �  j   �  �     E   �     �     �  $   �     #     +  ;   D  P   �  �   �  s   f     �     �     �  e                                                                 	                                                           
               Check for updates Easily manage your licenses for vendidero Products and enjoy automatic updates. Enter license key Error while requesting vendidero helper data. Find your license key Hide this notice It seems like the Update & Support Flatrate of one of your vendidero products expires in a few days: License Key Newest version: No vendidero products found. Once per week Oops, seems like the API needs a break Product Register Products Seems like your update- and support flat has expired. Please %s your license before updating. Sorry but currently we are not able to establish a connection to vendidero.de - please be patient and try again in a few minutes. Sorry, there was an error while unregistering %s Unregister Update & Support Update & Support Flatrate expires Version Welcome to vendidero Your %s license doesn't seem to be registered. Please %s Your %s license seems to have expired. Please %s Your update- and support-flat has expired. Please <a href="%s" target="_blank">renew</a> your license before updating. Your update- and support-flat has expired. Please renew your license before updating. check manage your licenses renew now vendidero Helper must be network activated when in multisite environment. Project-Id-Version: Vendidero Helper v1.0.3
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-07-16 10:54+0200
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.4.3
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 Nach Updates suchen Verwalte deine vendidero Lizenzen und profitiere von automatischen Updates. Lizenzschlüssel eingeben Fehler beim Abruf der API von vendidero. Finde deinen Lizenzschlüssel Nachricht ausblenden Es scheint so als würde die Update & Support Flatrate für eines deiner vendidero Produkte in Kürze ablaufen: Lizenzschlüssel Aktuelle Version: Keine vendidero Produkte gefunden. Einmal pro Woche Hoppala, es scheint als sei die API gerade unerreichbar Produkt Produkte registrieren Es scheint als wäre deine Update- und Support-Flatrate ausgelaufen. Bitte %s deine Lizenz vor dem Update. Sorry, leider kann aktuell keine Verbindung zu vendidero.de aufgebaut werden - bitte habe etwas Geduld und versuche es in ein paar Minuten erneut. Sorry, leider gab es beim deaktivieren der Lizenz von %s ein Problem. deaktivieren Updates & Support Update & Support Flatrate läuft aus Version Willkommen bei vendidero Deine %s Lizenz scheint nicht registriert zu sein. Bitte %s Deine Update- und Support-Flatrate für %s scheint ausgelaufen zu sein. Bitte %s Es scheint als wäre deine Update- und Support-Flatrate ausgelaufen. Bitte <a href="%s" target="_blank">verlängere</a> deine Lizenz vor dem Update. Es scheint als wäre deine Update- und Support-Flatrate ausgelaufen. Bitte verlängere deine Lizenz vor dem Update. prüfe verwalte deine Lizenzen jetzt verlängern Der vendidero Helper muss netzwerkweit aktiviert sein wenn du eine Multisite-Installation verwendest. 