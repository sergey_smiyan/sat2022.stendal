<?php

add_action('after_setup_theme', 'setup_theme_actions');
function setup_theme_actions()
{
    show_admin_bar(false);
    register_nav_menu('top-menu', 'Hauptmenü');
    register_nav_menu('foot-menu', 'Fußzeilenmenü');

    add_theme_support('post-thumbnails');
    add_theme_support('editor-styles');
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-lightbox' );

    add_editor_style('editor-style.css');
}

add_filter('jpeg_quality', 'my_filter_img');
function my_filter_img($quality)
{
    return 100;
}

include_once 'functions/acf.php';
include_once 'functions/scripts_n_styles.php';
include_once 'functions/Menu.php';

remove_filter('the_content', 'wptexturize');

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

/*add_filter( 'template_include', 'my_templates' );
function my_templates( $template ) {
    if( is_home() || is_category() ){
        if ( $new_template = locate_template( array( 'news.php' ) ) )
            return $new_template ;
    }
    return $template;
}*/

add_action( 'init', function () {
    remove_action( 'woocommerce_review_order_after_payment', 'woocommerce_gzd_template_render_checkout_checkboxes', 10 );
    add_action( 'woocommerce_gzd_review_order_before_submit', 'woocommerce_gzd_template_render_checkout_checkboxes', 10 );
}, 50 );

add_filter( 'woocommerce_bacs_account_fields', function ($fields) {
    unset($fields['account_number']);
    unset($fields['sort_code']);
    return $fields;
} );

add_filter( 'woocommerce_order_item_quantity_html', function ($html, $item) {
    return ' <strong class="product-quantity">' . sprintf( '%s', $item->get_quantity() ) . '</strong>';
}, 10, 2 );

add_action( 'woocommerce_thankyou_bacs', function () {
    if (get_field('pay_info', 'option')) {
        echo '<p class="woocommerce-notice pay-info">' . get_field('pay_info', 'option') . '</p>';
    }
}, 999 );
