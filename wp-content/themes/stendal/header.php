<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <title><?= wp_get_document_title() ?></title>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="<?= get_stylesheet_directory_uri() ?>/assets/web/img/favicon.png" type="image/x-icon">



    <style>
        :root {
            --main_2: blue;
        }

        body:not(.page-loaded) * {
            -webkit-transition: none !important;
            -o-transition: none !important;
            transition: none !important;
        }

        .preloader {
            /*display: none;*/
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #fff;
            z-index: 10000;
            background-position: center;
            background-size: 57px auto;
            background-repeat: no-repeat;
            background-image: url("<?= get_stylesheet_directory_uri() ?>/assets/web/img/favicon.png");
        }
    </style>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="preloader"></div>

<div class="wrapper">
    <header class="<?= (is_front_page() ? 'main-head' : 'inner-head') ?>">
        <div class="head-top ">
            <div class="container-fluid">
                <div class="head-line">
                    <div class="head-cell">
                        <div class="logo-wrap">
                            <a href="/" class="logo">
                                <span><?= get_field( 'logo', 'option' ) ?></span>
                            </a>
                        </div>

                        <nav class="mnu-wrap">
                            <?php wp_nav_menu([
                                'theme_location' => 'top-menu',
                                'container' => false,
                                'menu_class' => 'main-mnu',
                                'depth' => 2,
                                //'items_wrap' => '%3$s',
                                'walker' => new Top_Nav_Menu()
                            ]); ?>
                            <!--<ul class="main-mnu">

                                <li>
                                    <div class="main-lnk">
                                        <a href="#">
                                            <span>Category 1</span>
                                        </a>
                                        <button type="button" class="btn-sub-mnu">
                                            <span>Category 1</span>
                                        </button>
                                    </div>

                                    <div class="sub-mnu">
                                        <ul>
                                            <li><a href="#">sub link 1</a></li>
                                            <li><a href="#">sub link 2</a></li>
                                            <li><a href="#">sub link 3</a></li>
                                            <li><a href="#">sub link 4</a></li>
                                            <li><a href="#">sub link 5</a></li>
                                            <li><a href="#">sub link 6</a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li>
                                    <div class="main-lnk">
                                        <a href="#">
                                            <span>Category 2</span>
                                        </a>
                                        <button type="button" class="btn-sub-mnu">
                                            <span>Category 2</span>
                                        </button>
                                    </div>

                                    <div class="sub-mnu">
                                        <ul>
                                            <li><a href="#">sub link 1</a></li>
                                            <li><a href="#">sub link 2</a></li>
                                            <li><a href="#">sub link 3</a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li>
                                    <div class="main-lnk">
                                        <a href="#">
                                            <span>Category 3</span>
                                        </a>
                                        <button type="button" class="btn-sub-mnu">
                                            <span>Category 3</span>
                                        </button>
                                    </div>

                                    <div class="sub-mnu">
                                        <ul>
                                            <li><a href="#">sub link 1</a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li><a href="#">Category 4</a></li>

                                <li>
                                    <div class="main-lnk">
                                        <a href="#">
                                            <span>Category 5</span>
                                        </a>
                                        <button type="button" class="btn-sub-mnu">
                                            <span>Category 5</span>
                                        </button>
                                    </div>

                                    <div class="sub-mnu">
                                        <ul>
                                            <li><a href="#">sub link 1</a></li>
                                            <li><a href="#">sub link 2</a></li>
                                        </ul>
                                    </div>
                                </li>

                            </ul>-->
                        </nav>

                        <a href="<?= wc_get_cart_url() ?>" class="head-cart-link"></a>
                    </div>

                    <div class="head-cell">
                        <button class="toggle-btn"><span></span></button>
                    </div>
                </div>
            </div>
        </div>
    </header><!-- header END -->