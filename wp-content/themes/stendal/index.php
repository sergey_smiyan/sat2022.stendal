<?php get_header();?>
    <div class="layout">
            <main class="page-wrap">
                <section class="<?php
                if (is_cart()) {
                    echo 'cart-section';
                } elseif (is_checkout()) {
	                echo 'checkout-section';
                }
                elseif (is_singular()) {
	                echo 'sect-txt';
                }
                ?>">
                    <div class="container">
                        <?php the_content(); ?>
                    </div>
                </section>
            </main><!-- page-wrap END -->
        </div><!-- layout -->
<?php get_footer();