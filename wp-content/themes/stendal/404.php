<?php get_header();?>
	<div class="layout">
		<main class="page-wrap">
			<section class="not-found-wrap">
				<div class="container">
					<div class="not-found">
						<h1>ENTSCHULDIGUNG</h1>
						<h2>FEHLER 404</h1>
						<p>Es sieht so aus, als könnten wir nicht finden, was Sie suchen.</p>
						<a href="/" class="btn">Home</a>
					</div>					
				</div>
			</section>
		</main><!-- page-wrap END -->
	</div><!-- layout -->
<?php get_footer();