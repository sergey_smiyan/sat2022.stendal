<?php get_header();?>
    <div class="layout">
            <main class="home-wrap">
                <div class="promo" style="background-image: url('<?= get_stylesheet_directory_uri() ?>/assets/web/img/home/promo-bg.jpg')"></div>
                <section class="sect-txt">
                    <div class="container">
                        <div class="txt-box">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </section>
                <section class="main-products-section">
                    <div class="container">
                        <div class="products-categories">
                            <h2>Produktkategorien</h2>
                            <?php echo do_shortcode('[product_categories]'); ?>
                        </div>
                        <div class="products-list">
                            <?php echo do_shortcode('[products columns="4"]'); ?>
                        </div>
                    </div>
                </section>
            </main><!-- home-wrap END -->
        </div><!-- layout -->
<?php get_footer();