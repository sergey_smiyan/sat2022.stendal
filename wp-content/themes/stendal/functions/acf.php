<?php

if (function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' => 'Info',
        'menu_title' => 'Info',
        'menu_slug' => 'info',
        'icon_url' => 'dashicons-clipboard',
        'position' => 3,
        'capability' => 'administrator',
    ]);
}
