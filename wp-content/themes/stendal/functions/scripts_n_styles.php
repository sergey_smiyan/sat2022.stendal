<?php
add_action('wp_enqueue_scripts', 'scripts_and_styles', 9999);
function scripts_and_styles () {

//Libs
	wp_enqueue_style('bootstrap-grid', get_stylesheet_directory_uri().'/assets/web/libs/bootstrap-4.1.3/css/bootstrap-grid.min.css');
	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri().'/assets/web/libs/bootstrap-4.1.3/css/bootstrap.min.css');
    wp_enqueue_style('select2', get_stylesheet_directory_uri().'/assets/web/libs/select2-4.0.12/dist/css/select2.min.css');

//Main
	wp_enqueue_style('style', get_stylesheet_directory_uri().'/assets/web/css/style.css');
	wp_enqueue_style('fixes', get_stylesheet_directory_uri().'/assets/web/css/fixes_css.css');

//Scripts
    wp_deregister_script('jquery-core');
    wp_register_script('jquery-core', get_stylesheet_directory_uri().'/assets/web/libs/jquery-3.3.1/jquery-3.3.1.min.js');
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate', get_stylesheet_directory_uri().'/assets/web/libs/jquery-3.3.1/jquery-migrate.min.js');
    wp_enqueue_script('bowser', get_stylesheet_directory_uri().'/assets/web/libs/bowser-master/src/bowser.min.js');
    wp_enqueue_script('bootstrap', get_stylesheet_directory_uri().'/assets/web/libs/bootstrap-4.1.3/js/bootstrap.min.js');
    wp_enqueue_script('select2', get_stylesheet_directory_uri().'/assets/web/libs/select2-4.0.12/dist/js/select2.min.js');

    wp_enqueue_script('app', get_stylesheet_directory_uri().'/assets/web/js/app.js');

}