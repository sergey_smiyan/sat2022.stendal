<?php get_header();?>
    <div class="layout">
            <main class="page-wrap">
                <section class="<?php
                echo is_product() ? 'product-single-section' : '';
                echo is_product_tag() ? 'product-tag-section' : '';
                echo is_product_category() ? 'product-category-section' : '';
                ?>">
                    <div class="container">
                        <?php woocommerce_breadcrumb(); ?>
                        <?php woocommerce_content(); ?>
                    </div>
                </section>
            </main><!-- page-wrap END -->
        </div><!-- layout -->
<?php get_footer();