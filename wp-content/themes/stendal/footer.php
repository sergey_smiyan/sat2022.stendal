<footer class="<?= (is_front_page() ? 'main-foot' : 'inner-foot') ?>">
    <div class="container-fluid">

        <div class="foot-line">

            <div class="foot-cell">
                <div class="address-wrap">
                    <?= get_field( 'footer', 'option' )['address'] ?>
                </div>
            </div>

            <div class="foot-cell">

                <div class="contacts-wrap">
                    <div class="social-wrap">
                        <?php
                            $instagram = get_field( 'footer', 'option' )['instagram'];
                            $facebook = get_field( 'footer', 'option' )['facebook'];
                            $youtube = get_field( 'footer', 'option' )['youtube'];
                        ?>
                        <?php if ($instagram) : ?>
                        <a href="<?= $instagram ?>">
                            <img src="<?= get_stylesheet_directory_uri() ?>/assets/web/img/_style/_svg/_social/instagram-brands.svg" alt="instagram-logo">
                        </a>
                        <?php endif; ?>

	                    <?php if ($facebook) : ?>
                        <a href="<?= $facebook ?>">
                            <img src="<?= get_stylesheet_directory_uri() ?>/assets/web/img/_style/_svg/_social/facebook-brands.svg" alt="facebook-logo">
                        </a>
	                    <?php endif; ?>

	                    <?php if ($youtube) : ?>
                        <a href="<?= $youtube ?>">
                            <img src="<?= get_stylesheet_directory_uri() ?>/assets/web/img/_style/_svg/_social/youtube-brands.svg" alt="youtube-logo">
                        </a>
                        <?php endif; ?>
                    </div>

                    <div class="foot-nav">
                        <a href="#" data-toggle="modal" data-target="#kontakt-modal">Kontakt</a>
                        <?php wp_nav_menu([
                            'theme_location' => 'foot-menu',
                            'container' => false,
                            'depth' => 1,
                            'items_wrap' => '%3$s',
                            'walker' => new Foot_Nav_Menu()
                        ]); ?>
                    </div>
                </div>

            </div>

        </div>

    </div>
</footer><!-- footer END -->

<div class="modal-set">
    <!-- <button class="btn" data-toggle="modal" data-target="#example-modal">Button</button> -->

    <div class="modal fade" id="kontakt-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-top">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <?php echo do_shortcode( '[cf7form cf7key="kontakt"]' ); ?>
                </div>
            </div>
        </div>
    </div><!-- Modal END -->

</div><!-- modal-set END -->

<div class="loaded"></div>
<div class="btn-top"></div>
</div><!-- wrapper END -->
<?php wp_footer(); ?>
</body>
</html>