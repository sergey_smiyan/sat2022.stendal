$(document).ready(function () {
	function is_touch_enabled() { 
		return ( 'ontouchstart' in window ) ||  
		( navigator.maxTouchPoints > 0 ) ||  
		( navigator.msMaxTouchPoints > 0 ); 
	} 

	if( is_touch_enabled() ) { 
		$("html").addClass("touch");
	} 
	else { 
		$("html").addClass("no-touch");
	} 

	// Definition of browser
	if (bowser.mac){$("html").addClass("mac-os");}
	if (bowser.windows){$("html").addClass("win-os");}
	if (bowser.safari){$("html").addClass("safari");}
	if (bowser.chrome){$("html").addClass("chrome");}
	if (bowser.msie){$("html").addClass("int-expl");}
	if (bowser.gecko){$("html").addClass("mozz");}
	if (/Edge/.test(navigator.userAgent)) {$("html").addClass("edge");}
	// Definition of browser END

	$(".preloader").delay(50).fadeOut();
	setTimeout(function(){
		$("body").addClass("page-loaded");
	}, 150);


	// Bootstrap Modal
	$('.modal').modal({
		show: false
	});


	// Button Up, Help Button
	$(".btn-top").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

	$(window).scroll(function(){
		if ( $(document).scrollTop() > 200 ) {
			$('.btn-top').fadeIn('fast');
			$('.btn-top').addClass('on');
			$('.main-head, .inner-head').addClass('fixed');

		} else {
			$('.btn-top').fadeOut('fast');
			$('.btn-top').removeClass('on');
			$('.main-head, .inner-head').removeClass('fixed');
		}
	});
	// Button Up END

	// Cust select
	function custSel(){
		$(".cust-sel").each(function () {
			var thisParent  = $(this).children("span:not(.sel-title)");
			var custSel  = $(this).find('select');
			var thisAttr  = $(this).attr('data-placeholder');

			custSel.attr('data-placeholder', thisAttr);

			setTimeout(function(){
				custSel.select2({
					dropdownParent: thisParent,
					minimumResultsForSearch: 100,
					//multiple: true,
				});
			}, 50);
		});
	}
	custSel();
	// Cust select END

	// Toggle menu

	$(document).on("click", ".toggle-btn", function (e) {
		if ($(this).hasClass("on")) {
			$(".toggle-btn").removeClass("on");
			$(".mnu-wrap").fadeOut();
			$(".mnu-wrap").removeClass("open");
			$("body").removeClass("mnu-open");
		} else {
			$(this).addClass("on");
			$(".mnu-wrap").fadeIn();
			//$(".mnu-wrap").css("display", "flex").hide().fadeIn();
			$(".mnu-wrap").addClass("open");
			$("body").addClass("mnu-open");
		}
	});


	// Main menu
	if ($("html").hasClass("touch")) {

		$(".main-mnu .btn-sub-mnu").click(function () {
			if ($(this).parent(".main-lnk").parent("li").hasClass("hover")) {
				$(".main-mnu, .main-mnu li").removeClass("hover");
				$(".sub-mnu").slideUp();

			} else {
				$(".main-mnu li").removeClass("hover");
				$(".sub-mnu").slideUp();

				$(".main-mnu").addClass("hover");
				$(this).parent(".main-lnk").parent("li").addClass("hover");
				$(this).parent(".main-lnk").next(".sub-mnu").slideDown();
			}
		});

	} else {

		if ($(window).width() >= 768) {
			
			$(".main-mnu > li, .main-mnu").hover(function () {
				if ($(this).hasClass("hover")) {
					$(this).removeClass("hover");
				} else {
					$(this).addClass("hover");
				}
			});

		} else {

			$(".main-mnu .btn-sub-mnu").click(function () {
				if ($(this).parent(".main-lnk").parent("li").hasClass("hover")) {
					$(this).parent(".main-lnk").parent("li").removeClass("hover");
					$(".sub-mnu").slideUp();
				} else {
					$(this).parent(".main-lnk").parent("li").addClass("hover");
					$(this).parent(".main-lnk").next(".sub-mnu").slideDown();
				}
			});

		}

	}
	// Main menu END



	$("a[href='#kontakt-modal']").click(function (e){
		e.preventDefault();
		$("#kontakt-modal").modal("show");
	})


});
// Document ready END
